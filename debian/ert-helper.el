(setq ert-batch-backtrace-right-margin 500)

(custom-set-variables '(lsp-session-file "/tmp/lsp-test-session"))

(load-file "test/test-helper.el")

(ert-run-tests-batch-and-exit
 '(not (or
        (tag org)
        (tag no-win)
        ))
 )
