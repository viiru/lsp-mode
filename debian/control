Source: lsp-mode
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Build-Depends:
 clangd,
 debhelper-compat (= 13),
 dh-elpa,
 elpa-deferred,
 elpa-el-mock,
 elpa-f,
 elpa-ht,
 elpa-lv,
 elpa-markdown-mode,
 elpa-spinner,
 elpa-undercover,
 python3-pylsp,
Standards-Version: 4.6.0
Homepage: https://github.com/emacs-lsp/lsp-mode
Vcs-Browser: https://salsa.debian.org/emacsen-team/lsp-mode
Vcs-Git: https://salsa.debian.org/emacsen-team/lsp-mode.git
Testsuite: autopkgtest-pkg-elpa
Rules-Requires-Root: no

Package: elpa-lsp-mode
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs,
 emacs25
Description: Emacs client/library for the Language Server Protocol
 A Emacs Lisp library for implementing clients for servers using Microsoft's
 Language Server Protocol (v3.0)[1].
 .
 The library is designed to integrate with existing Emacs IDE frameworks
 (completion-at-point, xref (beginning with Emacs 25.1), flycheck, etc).
 .
 [1]: https://github.com/Microsoft/language-server-protocol
